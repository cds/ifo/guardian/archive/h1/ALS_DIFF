from guardian import GuardState, GuardStateDecorator
import ISC_library
import ISC_GEN_STATES
import json
import time
import cdsutils
from timeout_utils import call_with_timeout as timeout

##################################################
# Prep file path for saving offsets
##################################################
alsDiffParamsPath = '/opt/rtcds/userapps/trunk/als/common/guardian/alsDiffParams.dat'
with open(alsDiffParamsPath, 'r') as alsDiffParamFile:
    alsDiffParamDict = json.load(alsDiffParamFile)


##################################################
# STATES
##################################################
nominal = 'SHUTTERED'
request = 'PREPARE'

class INIT(GuardState):
    request = False

    def main(self):
        return 'IR_FOUND'

class LOCKLOSS(GuardState):
    request = False
    def run(self):
        return True


class DOWN(GuardState):
    index = 100
    goto = True

    def main(self):
        self.first_run = 1;
        ezca['LSC-DARM1_GAIN'] = 0.0
        ezca['LSC-DARM2_GAIN'] = 0.0
        ezca['LSC-DARM1_RSET'] = 2
        ezca['LSC-DARM2_RSET'] = 2
        ezca['SUS-ETMY_L1_LOCK_L_TRAMP'] = 10.0
        ezca['SUS-ETMX_L1_LOCK_L_TRAMP'] = 10.0
        ezca['SUS-ETMY_L1_LOCK_L_GAIN'] = 0.0
        for osem in ['UL', 'UR', 'LL', 'LR']:
            ezca['SUS-ETMX_BIO_L1_%s_STATEREQ'%osem] = 1 # Reverted to state 1, 3Dec2019 JCD
        ezca['SUS-ETMX_M0_TEST_L_OFFSET'] = 0
        ezca.switch('SUS-ETMX_L1_LOCK_L','FM2','FM1', 'FM9','OFF')

        # Turn off the high-bandwidth DIFF configuration
        # used during CARM/DARM transitioning.
        ezca.get_LIGOFilter('LSC-DARM1').only_on('INPUT', 'OUTPUT','DECIMATION', 'FM7')
        ezca.get_LIGOFilter('LSC-DARM2').only_on('INPUT', 'FM3','FM4','FM6', 'FM7', 'FM9', 'FM10', 'OUTPUT', 'DECIMATION')

        # set back to the nominal VCO offset.
        ezca['ALS-C_DIFF_VCO_TUNEOFS'] = -1.9
        #unpark VCO
        ezca['ALS-C_DIFF_PLL_PRESET'] = 'No'
        self.timer['ramp'] = 5

    def run(self):
        if self.timer['ramp'] and ezca['SUS-ETMX_L1_LOCK_L_OUTPUT'] > 10000:
            ezca['SUS-ETMX_L3_LOCK_L_RSET'] = 2.0
            ezca['SUS-ETMX_L1_LOCK_L_RSET'] = 2.0
            ezca['SUS-ETMX_L1_DRIVEALIGN_L2P_RSET'] = 2.0
            log('clearing ETMX history')
        if self.timer['ramp'] and self.first_run:
            ezca['SUS-ETMY_L1_LOCK_L_RSET'] = 2.0
            ezca['SUS-ETMX_L1_LOCK_L_RSET'] = 2.0
            ezca['SUS-ETMX_L1_DRIVEALIGN_L2P_RSET'] = 2.0
            time.sleep(0.1)
            self.first_run = 0
        if self.timer['ramp'] and not self.first_run:
            return True


class PREPARE(GuardState):
    index = 1

    @ISC_library.assert_dof_locked_gen(['IMC', 'XARM_GREEN', 'YARM_GREEN'])
    def main(self):
        # prepare DIFF PLL
        log('Setting DIFF PLL parameters')
        ezca['ALS-C_DIFF_PLL_INEN'] = 1
        ezca['ALS-C_DIFF_PLL_POL'] = 0
        ezca['ALS-C_DIFF_PLL_GAIN'] = 6
        ezca['ALS-C_DIFF_PLL_COMP1'] = 1
        ezca['ALS-C_DIFF_PLL_COMP2'] = 1
        ezca['LSC-DARM2_GAIN'] = 1
        ezca.switch('ALS-C_DIFF_PLL_CTRL', 'FM3', 'FM4', 'FM5', 'FM6', 'ON')
        ezca.switch('ALS-C_DIFF_PLL_CTRL', 'OFFSET', 'OFF')

        # prepare LSC
        log('Preparing the LSC')
        self.timer['wait'] = 0.5
        ezca['LSC-PD_DOF_MTRX_TRAMP'] = 0.0
        ezca['LSC-ARM_INPUT_MTRX_TRAMP'] = 0.0
        # clear output matrix
        ISC_library.outrix.put('ETMX',[],0)
        ISC_library.outrix.put('ETMY',[],0)
        ISC_library.darmcarm_outrix.put('ETMX',[],0)
        ISC_library.darmcarm_outrix.put('ETMY',[],0)

        # set output matrix
        ISC_library.darmcarm_outrix['ETMX', 'DARM'] = 1

        # make sure ISCINF is 1 in ETMX and 0 in ETMY
        ezca['SUS-ETMX_L3_ISCINF_L_GAIN']=1
        ezca['SUS-ETMY_L3_ISCINF_L_GAIN']=0

        ISC_library.intrix.zero(row='DARM') # clear DARM
        ISC_library.intrix_OMCAS45['DARM', 'ALS_DIFF'] = 1.0
        time.sleep(0.1)
        ISC_library.intrix_OMCAS45.load()
        ISC_library.intrix.load()

        # prepare ETMX suspension
        log('Preparing ETMX suspension')
        ezca.get_LIGOFilter('SUS-ETMX_L1_LOCK_L').only_on('INPUT', 'FM8', 'FM5', 'OUTPUT', 'DECIMATION')
        ezca['SUS-ETMX_L1_LOCK_L_GAIN'] = 0.4
        #ezca.get_LIGOFilter('SUS-ETMX_L1_LOCK_L').only_on('INPUT','FM10', 'FM4', 'OUTPUT','DECIMATION')
        #ezca['SUS-ETMX_L1_LOCK_L_GAIN'] = 1.06
        ezca.get_LIGOFilter('SUS-ETMX_L2_LOCK_L').only_on('INPUT', 'OUTPUT', 'DECIMATION')
        ezca['SUS-ETMX_L2_LOCK_L_GAIN'] = 1
        #ezca.get_LIGOFilter('SUS-ETMX_L2_LOCK_L').only_on('INPUT', 'FM7','OUTPUT', 'DECIMATION') 
        #ezca['SUS-ETMX_L2_LOCK_L_GAIN'] = 15        
        ezca.get_LIGOFilter('SUS-ETMX_L2_DRIVEALIGN_L2L').only_on('INPUT', 'OUTPUT', 'DECIMATION')
        ezca.get_LIGOFilter('SUS-ETMX_L3_DRIVEALIGN_L2L').only_on('INPUT', 'OUTPUT', 'DECIMATION')
        ezca.get_LIGOFilter('SUS-ETMX_L3_LOCK_L').only_on('INPUT','FM2', 'FM6', 'OUTPUT','DECIMATION')
        ezca['SUS-ETMX_L3_DRIVEALIGN_L2L_GAIN'] = 1
        #ezca['SUS-ETMX_L2_LOCK_L_GAIN'] = 1
        ezca['SUS-ETMX_L1_LOCK_L_TRAMP'] = 2.0
        ezca['SUS-ETMY_L1_LOCK_L_TRAMP'] = 2.0
        #ezca['SUS-ETMX_L1_LOCK_L_GAIN'] = 0.4
        #ezca['SUS-ETMY_L1_LOCK_L_GAIN'] = 0.2  # unneeded for LHO.  DOWN sets to 0 @ LHO, and lownoise_ESD sets to 0.6. JCD 28Nov2018
        ezca['SUS-ETMX_L3_LOCK_L_GAIN'] = 1.0
        #commented out because it was causing drive to ETMY while DARM is locked on ETMX
        #ezca['SUS-ETMY_L3_LOCK_L_GAIN'] = 1.0
        ezca.switch('SUS-ETMX_M0_LOCK_L', 'INPUT', 'OFF')
        ezca.switch('SUS-ETMY_M0_LOCK_L', 'INPUT', 'OFF')
        ezca['LSC-DARM1_TRAMP'] = 5
        ezca['LSC-DARM2_GAIN'] = 1

    @ISC_library.assert_dof_locked_gen(['IMC', 'XARM_GREEN', 'YARM_GREEN'])
    def run(self):
        if abs(ezca['ALS-C_DIFF_PLL_CTRLMON']) >= 5:
            self.timer['wait'] = 0.5
            notify('WAITING for DIFF PLL to come in range')
        elif ezca['ALS-C_DIFF_A_DEMOD_RFMON'] < -29: #SED lowered from -26 to -29 July 29 2020# New Threshold of DIFF Beatnote JSK, JCD 2019-08-14
            notify('DIFF beatnote is low!')
        else:
            if self.timer['wait']:
                notify('ALS DIFF okay')
                return True


class LOCK(GuardState):
    index = 2
    request = False

    @ISC_library.assert_dof_locked_gen(['IMC', 'XARM_GREEN', 'YARM_GREEN'])
    def main(self):
        # ezca.switch('LSC-DARM1', 'FM5', 'ON') # RXA HY commented it out # turn on -20 dB filter module
        log('Turning on DARM1 Gain to 40')
        ezca['LSC-DARM1_TRAMP'] = 5 # RA HY 12/23/18
        ezca['LSC-DARM1_GAIN']  = 40 #400

        self.timer['wait'] = ezca['LSC-DARM1_TRAMP']
        self.counter = 1

    @ISC_library.assert_dof_locked_gen(['IMC', 'XARM_GREEN', 'YARM_GREEN'])
    def run(self):
        if self.counter == 1 and self.timer['wait']:
            ezca['LSC-DARM1_GAIN']  = 400 #400
            self.counter += 1
            self.timer['wait'] = 2*ezca['LSC-DARM1_TRAMP']

        if self.counter == 2 and self.timer['wait']:
            log('Engaging boosts')
            ezca.switch('SUS-ETMX_L1_LOCK_L', 'FM1', 'FM2', 'ON')
            self.counter += 1
            self.timer['wait'] = 3

        if self.counter == 3 and self.timer['wait']:
            log('More boosts!')
            ezca.switch('LSC-DARM2', 'FM5', 'ON')
            self.counter += 1
            self.timer['wait'] = 10

        if self.counter == 4 and self.timer['wait']:
            log('done! locked!')
            return True


class LOCKED(GuardState):
    index = 3
    request = True

    @ISC_library.assert_dof_locked_gen(['IMC', 'XARM_GREEN', 'YARM_GREEN'])
    def main(self):
        ezca['ALS-C_DIFF_PLL_GAIN'] = 6

    @ISC_library.assert_dof_locked_gen(['IMC', 'XARM_GREEN', 'YARM_GREEN'])
    def run(self):
        return True

class PREP_SEARCH(GuardState):
    """Check that our QPD offset is acceptable. Otherwise, we will
    have a lower/higher signal than we actually do and confuse our
    static threshold values. A better fix would be to run the dark
    offset script to adjust the qpd seg offsets, but this seems to
    happen often and just needs a minor change that I think this is
    an okay solution - TJS May 2023

    This is a very similar state as in ALS_COMM, should probably
    make a common state gen at some point.
    """
    index = 9
    request = False

    @ISC_library.assert_dof_locked_gen(['IMC', 'XARM_GREEN', 'YARM_GREEN'])
    def main(self):
        self.acceptable_dark = 0.025
        # Set the PLL offset somewhere away incase we are already getting light
        ezca['ALS-C_DIFF_PLL_CTRL_TRAMP'] = 2
        ezca.switch('ALS-C_DIFF_PLL_CTRL', 'OFFSET', 'ON')
        ezca['ALS-C_DIFF_PLL_CTRL_OFFSET'] = 0
        self.timer['freq_move'] = 3
        self.freq_set = False

    def run(self):
        if self.timer['freq_move'] and not self.freq_set:
            self.freq_set = True
            return False
        elif self.freq_set:
            log('Getting 5sec of "dark" time')
            self.avg = timeout(cdsutils.avg, 5, 'H1:LSC-TR_Y_NORM_INMON')
            if abs(self.avg) > self.acceptable_dark:
                log('Need to reset TR Y QPD B offset')
                # I think just taking the average here should be good enough
                dark_avg = timeout(cdsutils.avg, 5, 'H1:LSC-TR_Y_QPD_B_SUM_INMON')
                ezca['LSC-TR_Y_QPD_B_SUM_OFFSET'] = -dark_avg.round(3)
                return True
            else:
                return True


# Possible offsets that can let you find a Y arm resonance
darmOffsetList = alsDiffParamDict['diffOffsets']

class FIND_IR(GuardState):
    index = 10
    request = True

    @ISC_library.assert_dof_locked_gen(['IMC', 'XARM_GREEN', 'YARM_GREEN'])
    def main(self):
        self.waitTime = 15
        ezca['ALS-C_DIFF_PLL_CTRL_TRAMP'] = self.waitTime # to make the offset ramp slow
        # Last state already set offset to 0
        # Grab a reference for the noise floor
        self.noise_avg = timeout(cdsutils.avg, 5, 'H1:LSC-TR_Y_NORM_OUTPUT')
        log(f'dark_noise_avg = {self.noise_avg}')
        self.timer['wait'] = 0
        self.counter = 0
        self.last_step = 0
        self.small_step = 30

    @ISC_library.assert_dof_locked_gen(['IMC', 'XARM_GREEN', 'YARM_GREEN'])
    def run(self):

        if self.timer['wait']:
            step_data = timeout(cdsutils.getdata, 'H1:LSC-TR_Y_NORM_OUTPUT', -20)
            if any(step_data.data):
                # Check if the avg of the last 5sec has acceptable buildup
                fivesec = step_data.data[int(step_data.sample_rate*-5):]
                avg = sum(fivesec)/len(fivesec)
                if avg > 0.08:
                    ezca['ALS-C_DIFF_PLL_CTRL_TRAMP'] = 2
                    return True
                # We passed a resonance in this step
                elif step_data.data.max() > 0.4:
                    log('Passed resonance, during last step')
                    # If there is some light, but not above threshold, take small step back
                    if avg - self.noise_avg > 0.025:
                        cur_offset = ezca['ALS-C_DIFF_PLL_CTRL_OFFSET']
                        if self.last_step >= 0:
                            # Should be OK to just subtract since these are now sorted, make last step negative
                            ezca['ALS-C_DIFF_PLL_CTRL_OFFSET'] = cur_offset - self.small_step
                            self.last_step = -self.small_step
                            self.timer['wait'] = 20
                            return False
                        # We've small stepped over it, half and positive
                        elif abs(self.last_step) == self.small_step:
                            ezca['ALS-C_DIFF_PLL_CTRL_OFFSET'] = cur_offset + (self.small_step/2)
                            self.last_step = self.small_step/2
                            self.timer['wait'] = 20
                            return False
                        # Perhaps time to give up here, keep trying or no ir found
                        # All values in list tried, no ir found
                        elif self.counter == len(darmOffsetList):
                            ezca['ALS-C_DIFF_PLL_CTRL_TRAMP'] = 2
                            return 'NO_IR_FOUND'
                            # Try next value
                        else:
                            ezca['ALS-C_DIFF_PLL_CTRL_OFFSET'] = darmOffsetList[self.counter]
                            self.timer['wait'] = self.waitTime + 5
                            self.last_step = darmOffsetList[self.counter] - self.last_step
                            self.counter += 1
                    # FIXME: Move to long search, this is temporary
                    # All values in list tried, no ir found
                    elif self.counter == len(darmOffsetList):
                        ezca['ALS-C_DIFF_PLL_CTRL_TRAMP'] = 2
                        return 'NO_IR_FOUND'
                        # Try next value
                    else:
                        ezca['ALS-C_DIFF_PLL_CTRL_OFFSET'] = darmOffsetList[self.counter]
                        self.timer['wait'] = self.waitTime + 5
                        self.last_step = darmOffsetList[self.counter] - self.last_step
                        self.counter += 1
                # All values in list tried, no ir found
                elif self.counter == len(darmOffsetList):
                    ezca['ALS-C_DIFF_PLL_CTRL_TRAMP'] = 2
                    return 'NO_IR_FOUND'
                    # Try next value
                else:
                    ezca['ALS-C_DIFF_PLL_CTRL_OFFSET'] = darmOffsetList[self.counter]
                    self.timer['wait'] = self.waitTime + 5
                    self.last_step = darmOffsetList[self.counter] - self.last_step
                    self.counter += 1




FINE_TUNE_IR = ISC_GEN_STATES.gen_TUNE_IR_BETTER(30, 5, 2, 0.55, 'LSC-TR_Y_NORM_INMON',
                           'ALS-C_DIFF_PLL_CTRL_OFFSET', 2, 2, ThreshLow=0.04)




class LONG_SEARCH_IR(GuardState):
    """Search around the known locations in small steps.
    It doesnt seem worth it to do a full scan since the offsets
    are always near a few different values +/- a bit
    """
    request = False
    index = 15
    @ISC_library.assert_dof_locked_gen(['IMC', 'XARM_GREEN', 'YARM_GREEN'])
    def main(self):
        self.known_spots = darmOffsetList
        # Search around the values in the offset list by +/- this amount
        self.search_range = 150
        self.step_size = 30
        self.small_step_tramp = 4
        self.big_step_tramp = 10
        self.power_threshold = 0.2
        # Setup ramp time for large move first
        ezca['ALS-C_DIFF_PLL_CTRL_TRAMP'] = self.big_step_tramp
        self.counter = 0
        # Status str options: init, searching, end of section, verify, complete
        self.status = 'init'
        self.timer['wait'] = 0

    # noinspection PyUnreachableCode
    @ISC_library.assert_dof_locked_gen(['IMC', 'XARM_GREEN', 'YARM_GREEN'])
    def run(self):
        if self.timer['wait']:

            # Add here checks for good buildups
            if ezca['LSC-TR_Y_NORM_INMON'] > self.power_threshold:
                log('Found power above threshold')
                self.status = 'verify'

            if self.status == 'init':
                log(f'Starting sweep around {darmOffsetList[self.counter]}')
                ezca['ALS-C_DIFF_PLL_CTRL_OFFSET'] = darmOffsetList[self.counter] - self.search_range
                self.timer['wait'] = self.big_step_tramp
                self.status = 'searching'
            elif self.status == 'searching':
                ezca['ALS-C_DIFF_PLL_CTRL_TRAMP'] = self.small_step_tramp
                ezca['ALS-C_DIFF_PLL_CTRL_OFFSET'] += self.step_size
                self.timer['wait'] = self.small_step_tramp + 0.5
                data = timeout(cdsutils.getdata, 'H1:LSC-TR_Y_NORM_OUTPUT', self.small_step_tramp)
                if data.data.max() > self.power_threshold:
                    log('Good power, moving to fine tune')
                    self.status = 'complete'
                    return True
                elif ezca['ALS-C_DIFF_PLL_CTRL_OFFSET'] >= darmOffsetList[self.counter] + self.search_range:
                    self.status = 'end of section'
                    self.counter += 1
            elif self.status == 'end of section':
                if self.counter >= len(darmOffsetList):
                    log('Exhausted full search, giving up')
                    return 'NO_IR_FOUND'
                else:
                    log('Moving to next search area')
                    ezca['ALS-C_DIFF_PLL_CTRL_TRAMP'] = self.big_step_tramp
                    ezca['ALS-C_DIFF_PLL_CTRL_OFFSET'] = darmOffsetList[self.counter] - self.search_range
                    self.timer['wait'] = self.big_step_tramp
                    self.status = 'searching'

            # Verify that a high power is still there after some time
            elif self.status == 'verify':
                log('Taking 5sec of data...')
                data = timeout(cdsutils.getdata, 'H1:LSC-TR_Y_NORM_OUTPUT', 5)
                if data.data.max() > self.power_threshold:
                    log('Good power, moving to fine tune')
                    self.status = 'complete'
                    return True
                else:
                    log('False peak, continuing to search')
                    self.status = 'searching'
            elif self.status == 'complete':
                notify('I am complete')
                return True
            # FIXME: This should do something better
            else:
                notify('End of the line!')
                return False


class SAVE_OFFSET(GuardState):
    index = 25
    request = False

    @ISC_library.assert_dof_locked_gen(['IMC', 'XARM_GREEN', 'YARM_GREEN'])
    def main(self):
        tr_avgs = cdsutils.avg(5, ['LSC-TR_Y_NORM_INMON', 'LSC-TR_X_NORM_INMON'])
        log(tr_avgs)
        self.NewOffset = ezca['ALS-C_DIFF_PLL_CTRL_OFFSET']
        log(self.NewOffset)
        self.Tolerance = 2000
        self.savedOffsetList = alsDiffParamDict['diffOffsets']
        log(self.savedOffsetList)
        self.writer = 0
        #write offset to alsparams here
        if tr_avgs[0] > 0.5 and tr_avgs[1] > 0.5:
            for ii in range(0, len(self.savedOffsetList)):
                log('checking against saved offset {} whose value is {}'.format(ii, self.savedOffsetList[ii]))
                if abs(self.NewOffset - self.savedOffsetList[ii]) < self.Tolerance and self.writer < 1:
                    log('Replacing saved offset of {} with new offset of {}. '
                        'Trans power is {}'.format(self.savedOffsetList[ii],
                                                       self.NewOffset,
                                                       tr_avgs[0]))
                    self.savedOffsetList[ii] = ezca['ALS-C_DIFF_PLL_CTRL_OFFSET']
                    # Remove duplicates and sort for searching in order
                    self.savedOffsetList = list(set(darmOffsetList))
                    # Reversing sorted order since the higher offsets seem to work better --TJS April 2024
                    self.savedOffsetList.sort(reverse=True)
                    # Write the new parameters to the file
                    alsDiffParamDict['diffOffsets'] = self.savedOffsetList
                    with open(alsDiffParamsPath, 'w+') as alsDiffParamFile:
                        json.dump(alsDiffParamDict, alsDiffParamFile)
                    self.writer = 1
                    return True
                else:
                    log('not saving offset here, too different from existing offsets')
                if ii == 1:
                    notify('Found offset is different from saved offsets '
                           'by more than {}'.format(self.Tolerance))
        elif tr_avgs[0] < 0.5:
            log('not saving offset since IR not high enough in Y arm')
        else:
            log('not saving offset since IR not high enough in X arm')


class IR_FOUND(GuardState):
    index = 26

    @ISC_library.assert_dof_locked_gen(['IMC', 'XARM_GREEN', 'YARM_GREEN'])
    def run(self):
            return True


class SHUTTERED(GuardState):
    index = 50
    goto = True
    
class IDLE(GuardState):
    index = 51
    goto = True


class NO_IR_FOUND(GuardState):
    index = 30
    request = False
    redirect = False

    @ISC_library.assert_dof_locked_gen(['IMC', 'XARM_GREEN', 'YARM_GREEN'])
    def run(self):
        if ezca['LSC-TR_Y_NORM_OUTPUT'] > 0.4:
            return True
        # So that we can re-request FIND_IR
        elif ezca['GRD-ALS_DIFF_REQUEST'] == 'FIND_IR':
            return True
        else:
            notify('Sorry!!! Search for IR Resonance by hand!')
            return False

##################################################
# EDGES
##################################################

edges = [
    ('DOWN', 'DOWN'),#adding an edge from DOWN to DOWN, this way when we are in DOWN and ISC_LOCK requests down,
    # the main will be run again.
    ('DOWN', 'PREPARE'),
    ('PREPARE', 'LOCK'),
    ('LOCK', 'LOCKED'),
    ('LOCKED', 'PREP_SEARCH'),
    #('PREP_SEARCH', 'FIND_IR'),
    ('PREP_SEARCH', 'LONG_SEARCH_IR'),
    #('FIND_IR', 'FINE_TUNE_IR'),
    ('LONG_SEARCH_IR', 'FINE_TUNE_IR'),
    ('FINE_TUNE_IR', 'SAVE_OFFSET'),
    ('SAVE_OFFSET', 'IR_FOUND'),
    ('IR_FOUND', 'LOCKED'),
    ('NO_IR_FOUND', 'SAVE_OFFSET'),
    ('SAVE_OFFSET', 'IR_FOUND'),
    ('IR_FOUND', 'SHUTTERED'),
    ('NO_IR_FOUND', 'FIND_IR'),
    ('IR_FOUND', 'IDLE'),
    ]
